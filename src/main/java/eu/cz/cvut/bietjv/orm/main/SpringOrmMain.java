package eu.cz.cvut.bietjv.orm.main;

import eu.cz.cvut.bietjv.orm.model.Product;
import eu.cz.cvut.bietjv.orm.service.ProductService;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;

import java.util.Arrays;

public class SpringOrmMain {//eu.cz.cvut.bietjv.orm
    public static void main(String[] args) {

        //Create Spring application context
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:/spring.xml");

        //Get service from context. (service's dependency (ProductDAO) is autowired in ProductService)
        ProductService productService = ctx.getBean(ProductService.class);

        //Do some data operation

        productService.add(new Product(6, "Bulb"));
        productService.add(new Product(7, "Dijone mustard"));

        System.out.println("listAll: " + productService.listAll());

        //Test transaction rollback (duplicated key)

        try {
            productService.addAll(Arrays.asList(new Product(8, "Book"), new Product(9, "Soap"), new Product(10, "Computer")));
        } catch (DataAccessException dataAccessException) {
        }

        //Test element list after rollback
        System.out.println("listAll: " + productService.listAll());

        ctx.close();

    }//eu.cz.cvut.bietjv.orm
}